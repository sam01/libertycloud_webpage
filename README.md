# libertycloud.de

The sourcecode of https://libertycloud.de.

No connections to third party services.

Thanks to https://github.com/iconmeister/iconmeister.github.io, https://feathericons.com/?query=cloud and https://fonts.google.com/specimen/Roboto.


## Features:
- Web Audio with [wavesurfer.js](https://wavesurfer-js.org/)

## Implemented music:
- Joe Hisaishi - One Summer's Day, played by [Magnus](https://github.com/vimpostor)